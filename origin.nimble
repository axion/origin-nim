# Package

version = "0.1.0"
author = "Michael Fiano"
description = "A graphics math library."
license = "MIT"
srcDir = "src"

# Dependencies

requires "nim >= 1.4.2"
