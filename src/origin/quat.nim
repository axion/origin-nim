import std/algorithm
import std/math
import std/random

import internal
import vec
import mat

type
  Quat* = object
    data*: Storage[4]

Quat.genAccessors w, x, y, z

proc `$`*(q: Quat): string =
  result = "["
  for i, c in q:
    result &= c.fmt
    if i < 3: result &= ", "
  result &= "]"

# Constructors

proc quat*(): Quat {.inline.} = result.data = [1f, 0f, 0f, 0f]
proc quat*(n: float32): Quat {.inline.} = result.data = [n, 0, 0, 0]
proc quat*(w, x, y, z: float32): Quat {.inline.} = result.data = [w, x, y, z]

# Constants

const quat_zero* = quat(0)
const quat_id* = quat(1)

# Operations

proc `==`*(x, y: Quat, rel = 1e-7, abs = 1e-7): bool {.inline.} =
  genComponentWiseBool(`~=`, x.data, y.data, rel, abs)

proc zero*[T: Quat](o: var T): var T {.inline.} =
  o.data.fill(0)
  result = o

proc rand*[T: Quat](o: var T, range = 0f..1f): var T {.inline.} =
  for x in o.mitems: x = rand(range)
  result = o
proc rand*[T: Quat](t: typedesc[T], range = 0f..1f): T {.inline.} = result.rand(range)

proc id*[T: Quat](o: var T): var T {.inline.} =
  discard o.zero
  o.w = 1
  result = o

proc `+`*[T: Quat](o: var T, a, b: T): var T {.inline.} =
  for i, x in o.mpairs: x = a[i] + b[i]
  result = o
proc `+`*[T: Quat](a, b: T): T {.inline.} = result.`+`(a, b)

proc `-`*[T: Quat](o: var T, a, b: T): var T {.inline.} =
  for i, x in o.mpairs: x = a[i] - b[i]
  result = o
proc `-`*[T: Quat](a, b: T): T {.inline.} = result.`-`(a, b)

proc `-`*[T: Quat](o: var T): var T {.inline.} =
  for i, x in o.mpairs: x = -o[i]
  result = o
proc `-`*[T: Quat](q: T): T {.inline.} = result = q; discard -result

proc `*`*[T: Quat](o: var T, a, b: T): var T {.inline.} =
  o.w = a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z
  o.x = a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y
  o.y = a.w * b.y + a.y * b.w + a.z * b.x - a.x * b.z
  o.z = a.w * b.z + a.z * b.w + a.x * b.y - a.y * b.x
  result = o
proc `*`*[T: Quat](a, b: T): T {.inline.} = result.`*`(a, b)

proc `*`*[T: Quat](o: var T, q: T, scalar: float32): var T {.inline.} =
  for i, x in o.mpairs: x = q[i] * scalar
  result = o
proc `*`*[T: Quat](q: T, scalar: float32): T {.inline.} = result.`*`(q, scalar)

proc conjugate*[T: Quat](o: var T, q: T): var T {.inline.} =
  o.w = q.w
  o.x = -q.x
  o.y = -q.y
  o.z = -q.z
  result = o
proc conjugate*[T: Quat](q: T): T {.inline.} = result.conjugate(q)

proc cross*[T: Quat](o: var T, a, b: T): var T {.inline.} =
  discard o.`*`(b * a.conjugate + a * b, 0.5)
  result = o
proc cross*[T: Quat](a, b: T): T {.inline.} = result.cross(a, b)

proc lenSq*(q: Quat): float32 {.inline.} =
  for x in q: result += x^2

proc len*(q: Quat): float32 {.inline.} = q.lenSq.sqrt

proc normalize*[T: Quat](o: var T, q: T): var T {.inline.} =
  let len = q.len
  if len != 0: result = o.`*`(q, 1/len)
  else: result = o.zero
proc normalize*[T: Quat](q: T): T {.inline.} = result.normalize(q)

proc dot*(a, b: Quat): float32 {.inline.} =
  for i, x in a: result += x * b[i]

proc inverse*[T: Quat](o: var T, q: T): var T {.inline.} =
  discard o.conjugate(q)
  o.`*`(o, 1/q.lenSq)
proc inverse*[T: Quat](q: T): T {.inline.} = result.inverse(q)

proc rotate*[T: Quat](o: var T, a, b: T, space = Space.local): var T {.inline.} =
  case space
  of Space.local: o = a * b
  of Space.world: o = b * a
  o.normalize(o)
proc rotate*[T: Quat](a, b: T, space = Space.local): T {.inline.} = result.rotate(a, b, space)

proc rotateEuler*[T: Quat](o: var T, q: T, angle: Vec3, space = Space.local): var T {.inline.} =
  let
    v = angle * 0.5
    s = v.sin
    c = v.cos
  o.w = c.x * c.y * c.z - s.x * s.y * s.z
  o.x = s.x * c.y * c.z + c.x * s.y * s.z
  o.y = c.x * s.y * c.z - s.x * c.y * s.z
  o.z = s.x * s.y * c.z + c.x * c.y * s.z
  o.rotate(q, o)
proc rotateEuler*[T: Quat](q: T, angle: Vec3, space = Space.local): T {.inline.} =
  result.rotateEuler(q, angle, space)

proc toEulerAngle*[T: Vec3](o: var T, q: Quat): var T {.inline.} =
  let
    sinr_cosp = (q.w * q.x + q.y * q.z) * 2
    cosr_cosp = 1 - (q.x ^ 2 + q.y ^ 2) * 2
    sin_p = (q.w * q.y - q.z * q.x) * 2
    siny_cosp = (q.w * q.z + q.x * q.y) * 2
    cosy_cosp = 1 - (q.y ^ 2 + q.z ^ 2) * 2
  o.x = arctan2(sinr_cosp, cosr_cosp)
  o.y = if sin_p.abs >= 1: Pi/2 * sin_p.cmp(0).float else: sin_p.arcsin
  o.z = arctan2(siny_cosp, cosy_cosp)
  result = o
proc toEulerAngle*(q: Quat): Vec3 {.inline.} = result.toEulerAngle(q)

proc fromAxisAngle*[T: Quat](o: var T, axis: Vec3, angle: float32): var T {.inline.} =
  let
    halfAngle = angle * 0.5
    s = halfAngle.sin
    c = halfAngle.cos
  o.w = c
  o.x = axis.x * s
  o.y = axis.y * s
  o.z = axis.z * s
  result = o
proc fromAxisAngle*(axis: Vec3, angle: float32): Quat {.inline.} = result.fromAxisAngle(axis, angle)

proc toMat3*[T: Mat3](o: var T, q: Quat): var T {.inline.} =
  let
    xx = q.x * q.x
    xy = q.x * q.y
    xz = q.x * q.z
    xw = q.x * q.w
    yy = q.y * q.y
    yz = q.y * q.z
    yw = q.y * q.w
    zz = q.z * q.z
    zw = q.z * q.w
  o.m00 = 1 - ((yy + zz) * 2)
  o.m10 = (xy + zw) * 2
  o.m20 = (xz - yw) * 2
  o.m01 = (xy - zw) * 2
  o.m11 = 1 - ((xx + zz) * 2)
  o.m21 = (yz + xw) * 2
  o.m02 = (xz + yw) * 2
  o.m12 = (yz - xw) * 2
  o.m22 = 1 - ((xx + yy) * 2)
  result = o
proc toMat3*(q: Quat): Mat3 {.inline.} = result.toMat3(q)

proc toMat4*[T: Mat4](o: var T, q: Quat): var T {.inline.} =
  let
    xx = q.x * q.x
    xy = q.x * q.y
    xz = q.x * q.z
    xw = q.x * q.w
    yy = q.y * q.y
    yz = q.y * q.z
    yw = q.y * q.w
    zz = q.z * q.z
    zw = q.z * q.w
  o.m00 = 1 - ((yy + zz) * 2)
  o.m10 = (xy + zw) * 2
  o.m20 = (xz - yw) * 2
  o.m30 = 0.0
  o.m01 = (xy - zw) * 2
  o.m11 = 1 - ((xx + zz) * 2)
  o.m21 = (yz + xw) * 2
  o.m31 = 0.0
  o.m02 = (xz + yw) * 2
  o.m12 = (yz - xw) * 2
  o.m22 = 1 - ((xx + yy) * 2)
  o.m32 = 0.0
  o.m03 = 0.0
  o.m13 = 0.0
  o.m23 = 0.0
  o.m33 = 1.0
  result = o
proc toMat4*(q: Quat): Mat4 {.inline.} = result.toMat4(q)

proc fromMat*[T: Quat](o: var T, m: Mat3 or Mat4): var T =
  o.w = sqrt(max(0, 1 + m.m00 + m.m11 + m.m22)) * 0.5
  o.x = sqrt(max(0, 1 + m.m00 - m.m11 - m.m22)) * (m.m21 - m.m12).cmp(0).float * 0.5
  o.y = sqrt(max(0, 1 - m.m00 + m.m11 - m.m22)) * (m.m02 - m.m20).cmp(0).float * 0.5
  o.z = sqrt(max(0, 1 - m.m00 - m.m11 + m.m22)) * (m.m10 - m.m01).cmp(0).float * 0.5
  result = o
proc fromMat*(m: Mat3 or Mat4): Quat {.inline.} = result.fromMat(m)

proc slerp*[T: Quat](o: var T, a, b: T, factor: float32): var T =
  var
    dot = dot(a, b)
    b = b
  if dot < 0:
    discard -b
    dot = -dot
  if dot.abs > 0.9995:
    for i, x in o.mpairs: x = lerp(a[i], b[i], factor)
  else:
    let
      angle = dot.arccos
      sinAngle = angle.sin
      scale1 = sin(angle * (1 - factor)) / sinAngle
      scale2 = sin(factor * angle) / sinAngle
    for i, x in o.mpairs: x = a[i] * scale1 + b[i] * scale2
  result = o
proc slerp*[T: Quat](a, b: T, factor: float32): T {.inline.} = result.slerp(a, b, factor)

proc orient*[T: Quat](o: var T, space = Space.local,
                      axes_angles: varargs[(Axis3d, float32)]): var T {.inline.} =
  var q = quat(1)
  var v = vec3()
  result = o
  discard o.id
  for (axis, angle) in axes_angles:
    case axis
    of Axis3d.X:
      v.x = 1; v.y = 0; v.z = 0
    of Axis3d.Y:
      v.x = 0; v.y = 1; v.z = 0
    of Axis3d.Z:
      v.x = 0; v.y = 0; v.z = 1
    # Make the individual quaternion rotation to represent the axis/angle representation.
    discard q.fromAxisAngle(v, angle)
    # Update the accumulating rotation, carefully minding the multiplication order to ensure the
    # final rotation we compute applies in right-to-left order.
    # Note: That means the order for this multiply should be total <- total * current, because
    # we're using a reduction-like pass to associate left to right multiplications. But, since we're
    # keeping the ultimate order of the applications the same as the argument order, the final
    # rotation applies right to left.
    case space
    of Space.local: o = q * o
    of Space.world: o = o * q
    # Ensure it is normalized for the next concatenation.
    discard o.normalize(o)
proc orient*(space = Space.local, axes_angles: varargs[(Axis3d, float32)]): Quat {.inline.} =
  result.orient(space, axes_angles)

proc fromVelocity*[T: Quat](o: var T, velocity: Vec3, delta: float32): var T {.inline.} =
  discard o.fromAxisAngle(velocity.normalize, velocity.len * delta).normalize
  result = o
proc fromVelocity*(velocity: Vec3, delta: float32): Quat {.inline.} =
  result.fromVelocity(velocity, delta)

var a = mat4()
let b = quat_id.rotateEuler(vec3(Pi/3, 0, 0))
echo b
echo a.toMat4(b)
